import { Router } from 'express';
import config from '../config/config.js';

const router = Router();

router.get('/', (req, res) => {
  const messages = config.data.messages;
  res.send(messages);
});

router.post('/', async (req, res) => {
  const message = req.body;

  config.data.messages.push(message);
  await config.write();

  res.send(message);
});

router.put('/', async (req, res) => {
  const message = req.body;

  config.data.messages = config.data.messages.map((i) => {
    if (i.id === message.id) {
      return message;
    }
    return i;
  });

  await config.write();
  res.json(message);
});

router.delete('/:id', async (req, res) => {
  config.data.messages = config.data.messages.filter(
    (i) => i.id !== req.params.id
  );
  await config.write();
  res.json(req.params.id);
});

export default router;
