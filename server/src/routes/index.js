import messagesRoutes from './messages.routes.js';

export default (app) => {
  app.use('/api/messages', messagesRoutes);
};
