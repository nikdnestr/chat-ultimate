import React from 'react';

const HeaderItem = ({ text, className, position = null }) => {
  let style;
  if (!position) {
    style = { margin: '10px' };
  } else style = { margin: '10px 10px 10px auto' };

  return (
    <p className={className} style={style}>
      {text}
    </p>
  );
};

export default HeaderItem;
