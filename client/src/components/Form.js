import React, { useState } from 'react';
import { v4 as uuidv4 } from 'uuid';

export const Form = ({ handleSubmit }) => {
  const [input, setInput] = useState('');

  const now = new Date(Date.now());

  const addMessage = (e) => {
    e.preventDefault();
    const newMessage = {
      id: uuidv4(),
      userId: uuidv4(),
      avatar:
        'https://pbs.twimg.com/profile_images/1030941554763096065/clDF9QRY.jpg',
      user: 'Nik',
      text: `${input}`,
      createdAt: now.toJSON(),
      editedAt: '',
    };
    handleSubmit(newMessage);
    setInput('');
  };

  return (
    <form className="message-input" style={style.form} onSubmit={addMessage}>
      <div>
        <input
          className="message-input-text"
          style={style.input}
          value={input}
          onChange={(e) => setInput(e.target.value)}
          placeholder="To edit your message type something here and then press the edit button, yeah I know its stupid"
        />
        <input
          className="message-input-button"
          style={style.btn}
          type="submit"
          value="Send"
        />
      </div>
    </form>
  );
};

const style = {
  form: {
    padding: '20px 50px 0 50px',
  },
  input: {
    width: 'calc(100% - 55px)',
    boxSizing: 'border-box',
    padding: '5px',
    border: 'black 1px solid',
    borderRight: 'none',
    outline: 'none',
    background: '#fff',
  },
  btn: {
    background: '#fff',
    borderLeft: 'none',
    border: 'black 1px solid',
    outline: 'none',
    padding: '5px',
  },
};
