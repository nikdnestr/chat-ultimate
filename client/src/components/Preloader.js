import React from 'react';
import { useSelector } from 'react-redux';

export const Preloader = () => {
  const show = useSelector((state) => state.chat.preloader);

  const isLoading = show ? style.flex : style.none;

  return (
    <div className="preloader" style={{ ...style.container, ...isLoading }}>
      LOADING
    </div>
  );
};

const style = {
  container: {
    position: 'fixed',
    top: '0',
    bottom: '0',
    left: '0',
    right: '0',
    justifyContent: 'center',
    alignItems: 'center',
  },
  flex: {
    display: 'flex',
  },
  none: {
    display: 'none',
  },
};
