import React from 'react';
import { Message } from './Message';

export const MessageList = ({ messages, onDelete }) => {
  const msgArr = messages.map((msg) => {
    return <Message message={msg} onDelete={onDelete} key={msg.id} />;
  });

  return (
    <div>
      <div className="message-list" style={style}>
        {msgArr}
      </div>
    </div>
  );
};

const style = {
  marginTop: '10px',
  minHeight: '80vh',
  border: 'black 1px solid',
};
