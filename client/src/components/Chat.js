import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  getMessages,
  addMessage,
  deleteMessage,
} from '../redux/slices/messageActions';
import { Header } from './Header';
import { MessageList } from './MessageList';
import { Form } from './Form';
import { Preloader } from './Preloader';

const Chat = () => {
  const dispatch = useDispatch();
  const messages = useSelector((state) => state.chat.messages);
  const show = useSelector((state) => state.chat.preloader);

  useEffect(() => {
    dispatch(getMessages());
  }, [dispatch]);

  const handleSubmitMessage = (message) => dispatch(addMessage(message));
  const handleDeleteMessage = (id) => dispatch(deleteMessage(id));

  const content = (
    <div>
      <Header info={messages} />
      <MessageList messages={messages} onDelete={handleDeleteMessage} />
      <Form handleSubmit={handleSubmitMessage} />
    </div>
  );

  return (
    <div className="chat" style={style}>
      {show ? <Preloader /> : content}
    </div>
  );
};

const style = {
  backgroundColor: '#F4F1DE',
  padding: '50px 20vw',
};

export default Chat;
