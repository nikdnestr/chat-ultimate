import React from 'react';

export const Avatar = ({ src }) => {
  return (
    <img className="message-user-avatar" src={src} alt="avatar" style={style} />
  );
};

const style = {
  objectFit: 'cover',
  minWidth: '150px',
  maxWidth: '150px',
  height: '150px',
};
