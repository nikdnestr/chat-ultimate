import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useParams, useHistory } from 'react-router';
import { editMessage } from '../redux/slices/messageActions';

export const MessageEditView = () => {
  const messages = useSelector((state) => state.chat.messages);
  const [input, setInput] = useState('');
  const id = useParams().id;
  const history = useHistory();
  const dispatch = useDispatch();

  const onEditMessage = (e) => {
    e.preventDefault();
    const newMessage = {
      ...findMessage(messages, id),
      text: input,
      editedAt: Date.now(),
    };
    dispatch(editMessage(newMessage));
    history.push('/chat');
  };

  const onCancel = () => {
    history.push('/chat');
  };

  const findMessage = (arr, id) => {
    const message = arr.filter((item) => item.id === id);
    return message[0];
  };

  return (
    <form
      style={style.container}
      action="submit"
      onSubmit={(e) => onEditMessage(e)}
    >
      <div style={style.content.container}>
        <div style={style.content.title}>Edit Your Message</div>
        <textarea
          style={style.content.input}
          onChange={(e) => setInput(e.target.value)}
          cols="30"
          rows="10"
        ></textarea>
        <div>
          <button
            style={style.content.button}
            className="edit-message-button"
            type="submit"
          >
            Send
          </button>
          <button
            style={style.content.button}
            className="edit-message-close"
            type="button"
            onClick={onCancel}
          >
            Close
          </button>
        </div>
      </div>
    </form>
  );
};

const style = {
  container: {
    display: 'flex',
    position: 'fixed',
    top: '0',
    bottom: '0',
    left: '0',
    right: '0',
    justifyContent: 'center',
  },
  content: {
    container: {
      borderRadius: '25px',
      padding: '30px',
      marginTop: '200px',
      background: '#D13242',
      color: '#fff',
      maxHeight: '190px',
    },
    title: {
      marginBottom: '20px',
    },
    input: {
      width: '200px',
      height: '100px',
      padding: '5px',
      resize: 'none',
      border: 'none',
      borderRadius: '5px',
    },
    button: {
      margin: '5px 10px 0 0',
      padding: '10px',
      border: 'none',
      borderRadius: '5px',
      background: '#fff',
    },
  },
};
