import React, { useState } from 'react';
// import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';

export const LoginView = () => {
  const [login, setLogin] = useState('');
  const [password, setPassword] = useState('');
  // const dispatch = useDispatch();
  const history = useHistory();

  const onSubmit = (e) => {
    e.preventDefault();
    history.push('/chat');
  };

  return (
    <div style={styles.container}>
      <form onSubmit={onSubmit} action="">
        <div>
          <input
            value={login}
            onChange={(e) => setLogin(e.target.value)}
            placeholder="login"
            name="login"
            type="text"
          />
        </div>
        <input
          value={password}
          onChange={(e) => setPassword(e.target.value)}
          placeholder="password"
          name="password"
          type="text"
        />
        <button type="submit">kek</button>
      </form>
    </div>
  );
};

const styles = {
  container: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  input: {},
  button: {},
};
