import { createSlice } from '@reduxjs/toolkit';
import {
  getMessages,
  addMessage,
  editMessage,
  deleteMessage,
} from './messageActions';

const initialState = {
  messages: [],
  preloader: true,
};

export const messagesSlice = createSlice({
  name: 'messages',
  initialState,
  reducers: {},
  extraReducers: {
    [getMessages.pending]: (state, action) => {
      state.preloader = true;
    },
    [getMessages.fulfilled]: (state, action) => {
      state.preloader = false;
      state.messages = action.payload;
    },
    [getMessages.rejected]: (state, action) => {
      state.preloader = false;
    },
    [addMessage.fulfilled]: (state, action) => {
      state.messages.push(action.payload);
    },
    [editMessage.fulfilled]: (state, action) => {
      const message = action.payload;
      state.messages = state.messages.map((oldMessage) => {
        if (oldMessage.id === message.id) return message;
        return oldMessage;
      });
    },
    [deleteMessage.fulfilled]: (state, action) => {
      const id = action.payload;

      state.messages = state.messages.filter((message) => message.id !== id);
    },
  },
});

export default messagesSlice.reducer;
