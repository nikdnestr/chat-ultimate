import { createAsyncThunk } from '@reduxjs/toolkit';

const getMessages = createAsyncThunk('getMessages', async () => {
  const messages = await fetch('http://localhost:3001/api/messages')
    .then((res) => res.json())
    .then((result) => result);
  return messages;
});

const addMessage = createAsyncThunk('addMessage', async (message) => {
  await fetch('http://localhost:3001/api/messages', {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(message),
  });
  return message;
});

const editMessage = createAsyncThunk('editMessage', async (message) => {
  await fetch(`http://localhost:3001/api/messages`, {
    method: 'PUT',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(message),
  });
  return message;
});

const deleteMessage = createAsyncThunk('deleteMessage', async (id) => {
  await fetch(`http://localhost:3001/api/messages/${id}`, {
    method: 'DELETE',
    headers: { 'Content-Type': 'application/json' },
  });
  return id;
});

export { getMessages, addMessage, editMessage, deleteMessage };
