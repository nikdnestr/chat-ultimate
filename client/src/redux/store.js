import { configureStore, getDefaultMiddleware } from '@reduxjs/toolkit';

import messagesSlice from './slices/messageSlice';

const store = configureStore({
  reducer: {
    chat: messagesSlice,
  },
  middleware: getDefaultMiddleware({ thunk: true }),
});

export default store;
