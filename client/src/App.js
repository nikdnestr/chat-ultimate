import Chat from './components/Chat.js';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { LoginView } from './views/LoginView';
import { MessageEditView } from './views/MessageEditView';

const App = () => {
  return (
    <div style={styles}>
      <BrowserRouter>
        <Switch>
          <Route path="/edit/:id" component={MessageEditView} />
          <Route path="/chat" component={Chat} />
          <Route path="/" component={LoginView} />
        </Switch>
      </BrowserRouter>
    </div>
  );
};

const styles = {
  fontFamily: '"Quicksand", sans-serif',
};

export default App;
