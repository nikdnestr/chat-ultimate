import Chat from './src/components/Chat';
import { rootReducer } from './src/redux/store';

export default {
  Chat,
  rootReducer,
};
